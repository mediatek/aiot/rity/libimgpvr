#!/usr/bin/python3
###############################################################################
# @File         pvrhtbmerge.py
# @Title        HTB and FW log merging script
# @Copyright    Copyright (c) Imagination Technologies Ltd. All Rights Reserved
# @Description  Script to execute a set of tests and collate the benchmark
#               performance into a single report.
#
#               A Host Trace can be merged with a corresponding Firmware Trace.
#               This is achieved by inserting synchronisation data into both
#               traces and post processing to merge them.
#
#               The FW Trace will contain a "Sync Partition Marker". This is
#               updated every time the RGX is brought out of reset (RGX clock
#               timestamps reset at this point) and is repeated when the FW
#               Trace buffer wraps to ensure there is always at least 1
#               Partition Marker in the Firmware Trace buffer whenever it is
#               read.
#
#               The Host Trace will contain corresponding "Sync Partition
#               Markers". Each partition is then subdivided into "Sync Scale"
#               sections. The "Sync Scale" data allows the timestamps from the
#               two traces to be correlated. The "Sync Scale" data is updated as
#               part of the standard RGX time correlation code (rgxtimecorr.c)
#               and is updated periodically including on power and clock
#               changes.
#
# @License      Strictly Confidential.
###############################################################################

import sys
import re
import argparse

#############################
# Log syntax markers
#############################

SYNC_PARTITION_OFF=-1
SYNC_PARTITION_BASE=10

SYNC_PARTITION_MK="marker:"
SYNC_PARTITION_RPT="repeat:"
SYNC_SCALE_MK_SYNC="HTBFWMkSync"

SP_ID="Mark"
SP_BASE=10
SP_OFF=-1

OSTS_ID="OSTS"
OSTS_BASE=10
OSTS_OFF=-1

CRTS_ID="CRTS"
CRTS_BASE=10
CRTS_OFF=-1

CLKS_ID="CalcClkSpd"
CLKS_BASE=10
CLKS_OFF=-1

LOGTS_BASE=10

# Set this to True to see sync points in merged log
DEBUG = False

TIME_SCALE=1000

class HTBSyncPoint(object):
    '''
        Class used to represent sync points
    '''
    def __init__(self, osts, crts, calk_clk_spd):
        self.osts = float(osts)
        self.crts = int(crts)
        self.calk_clk_spd = int(calk_clk_spd)
        self.scale_factor = 256000000000/float(calk_clk_spd)
        self.offset_ns = (osts * 1000000000) - (crts * self.scale_factor)


def split_scale_params(line):
    '''
        Split the given sync marker string into each parameter,
        returns the split params.
    '''
    if not SYNC_SCALE_MK_SYNC in line:
        print("Sync data expected but not found, offending Host Trace line:")
        print("            %s" % line)
        return 0, 0, 0, 0

    for token in line.split():
        if SP_ID in token:
            sp = int(token.split('=')[SP_OFF], SP_BASE)
        elif OSTS_ID in token:
            os_ts = float(token.split('=')[OSTS_OFF])
        elif CRTS_ID in token:
            cr_ts = int(token.split('=')[CRTS_OFF], CRTS_BASE)
        elif CLKS_ID in token:
            spd = int(token.split('=')[CLKS_OFF], CLKS_BASE)
    return (sp, os_ts, cr_ts, spd)


def find_htb_sync_points(file):
    '''
        Find all HTB sync points in the given HTB file and add to
        a dict.
    '''
    sp_hash_tbl = {}
    for line in file:
        if SYNC_SCALE_MK_SYNC in line:
            sp, osts, crts, spd = split_scale_params(line)
            if not sp in sp_hash_tbl:
                if osts != 0 and crts != 0 and spd != 0:
                    sp_hash_tbl[sp] = HTBSyncPoint(osts, crts, spd)
    return sp_hash_tbl


def find_fw_sync_points(file):
    '''
        Find all FW sync points in the given FW file and add to
        a dict.
    '''
    sp_hash_tbl = {}
    for line in file:
        if SYNC_PARTITION_MK in line or SYNC_PARTITION_RPT in line:
            sp = int(line.split()[SYNC_PARTITION_OFF], SYNC_PARTITION_BASE)
            line_ts, sep, message = line.partition(':')
            sp_hash_tbl[sp] = None
    return sp_hash_tbl


def convert_fw_clock(clock, offset, scale):
    '''
        Convert timestamp from FW clock to real time.
    '''
    return (((float(offset) + (float(clock) * float(scale)))/TIME_SCALE)) / 1000000


def print_log_line(message, ts):
    '''
        Print a log line with timestamp, debug mode
        also prints sync markers to the log.
    '''
    if SYNC_SCALE_MK_SYNC in message:
        if DEBUG:
            print("%020.9f:%s" % (float(ts), message), end='')
    else:
        print("%020.9f:%s" % (float(ts), message), end='')


def read_line_ts(file, ts):
    '''
        Read and return a line if its timestamp is less than
        the one provided.
    '''
    pos = file.tell()
    line = file.readline()
    if line:
        try:
            line_ts, sep, message = line.partition(':')
            if not ts or float(line_ts) < ts:
                return (message, float(line_ts))
        except ValueError:
            return (line, 0)
    file.seek(pos)
    return ("", 0)


def get_last_ts(host_file, fw_file):
    '''
        Read through the HTB and FW files given and find the last
        timestamp.
    '''
    host_pos = host_file.tell()
    fw_pos = fw_file.tell()

    line = host_file.readline()
    prevline = ""
    while line:
        prev_line = line
        line = host_file.readline()
    line_ts, sep, message = prevline.partition(':')
    host_last = line_ts

    line = fw_file.readline()
    prev_line = ""
    while line: 
        prev_line = line
        line = fw_file.readline()
    line_ts, sep, message = prevline.partition(':')
    fw_last = line_ts

    host_file.seek(host_pos)
    fw_file.seek(fw_pos)
    return (host_last, fw_last)


def seek_first_ts(host_file, fw_file):
    '''
        Read through each file until the first valid timestamp
        is found
    '''
    host_line = host_file.readline()

    while host_line:
        try:
            host_line_ts, sep, message = host_line.partition(':')
            host_line_ts = float(host_line_ts)
            break
        except ValueError:
            host_line = host_file.readline()

    fw_line = fw_file.readline()

    while fw_line:
        try:
            fw_line_ts, sep, message = fw_line.partition(':')
            fw_line_ts = float(fw_line_ts)
            break
        except ValueError:
            fw_line = fw_file.readline()


def sync_block_loop(host_file, fw_file, current_sp, next_sp):
    '''
        Main loop of the system, loops between 2 given sync points
        and merges any timestamps between the 2.
    '''
    #get next scale marker for limit
    if next_sp:
        os_ts_limit = next_sp.osts
        fw_ts_limit = next_sp.crts
    else:
        os_ts_limit, fw_ts_limit = get_last_ts(host_file, fw_file)

    #read first line in prep for loop
    host_line, osts_sec = read_line_ts(host_file, os_ts_limit)
    fw_line, fwts_clk = read_line_ts(fw_file, fw_ts_limit)
    #Convert fw clock to OS time
    fwts_sec = convert_fw_clock(fwts_clk, current_sp.offset_ns, current_sp.scale_factor)

    while host_line and fw_line:

        if fwts_sec < osts_sec:
            print_log_line(fw_line, fwts_sec)
            fw_line, fwts_clk = read_line_ts(fw_file, fw_ts_limit)
            fwts_sec = convert_fw_clock(fwts_clk, current_sp.offset_ns, current_sp.scale_factor)
        else:
            print_log_line(host_line, osts_sec)
            host_line, osts_sec = read_line_ts(host_file, os_ts_limit)

    while fw_line:
        print_log_line(fw_line, fwts_sec)
        fw_line, fwts_clk = read_line_ts(fw_file, fw_ts_limit)
        fwts_sec = convert_fw_clock(fwts_clk, current_sp.offset_ns, current_sp.scale_factor)

    while host_line:
        print_log_line(host_line, osts_sec)
        host_line, osts_sec = read_line_ts(host_file, os_ts_limit)


def preamble(host_file, fw_file, f_host_point):
    '''
        Given the first common sync point, traverse to the
        first timestamp that is able to be merged.
    '''

    seek_first_ts(host_file, fw_file)

    host_line = host_file.readline()
    host_line_ts, sep, message = host_line.partition(':')
    host_line_ts = float(host_line_ts)

    while host_line_ts < f_host_point.osts:
        host_line = host_file.readline()
        host_line_ts, sep, message = host_line.partition(':')
        host_line_ts = float(host_line_ts)

    fw_line = fw_file.readline()
    fw_line_cr, sep, message = fw_line.partition(':')
    fw_line_ts = convert_fw_clock(fw_line_cr, f_host_point.offset_ns, f_host_point.scale_factor)

    while fw_line_ts < f_host_point.osts:
        fw_line = fw_file.readline()
        fw_line_cr, sep, message = fw_line.partition(':')
        fw_line_ts = convert_fw_clock(float(fw_line_cr), f_host_point.offset_ns, f_host_point.scale_factor)


def main():

    parser = argparse.ArgumentParser(description="HTB/FW Merge Script")
    parser.add_argument(metavar='FILE', dest='htb_log', action='store', help="Location of the HTB Log file")
    parser.add_argument(metavar='FILE', dest='fw_log', action='store', help="Location of the FW Log file")

    args = parser.parse_args()

    try:
        with open(args.htb_log) as host_file, open(args.fw_log) as fw_file:

            #First iterate through the file finding all the sync points
            htb_points = find_htb_sync_points(host_file)
            fw_points = find_fw_sync_points(fw_file)

            #rewind
            host_file.seek(0)
            fw_file.seek(0)

            #get first item in dictionary
            htb_point = min(htb_points)
            fw_point = min(fw_points)

            #find lowest common marker
            while htb_point != fw_point and fw_point in fw_points and htb_point in htb_points:
                if fw_point < htb_point:
                    fw_point += 1
                if fw_point > htb_point:
                    htb_point += 1    

            if (htb_point != fw_point):
                print("No common marker found, merge not possible")
                exit(1)

            #Preamble from start of file
            preamble(host_file, fw_file, htb_points[htb_point])

            #loop until EOF
            while True:
                h_line = host_file.readline()
                f_line = fw_file.readline()
                if not h_line:
                    break
                if not f_line:
                    break
                if not htb_point:
                    break

                next_htb_point = None
                if htb_point:
                    if htb_point + 1 in htb_points:
                        next_htb_point = htb_point + 1

                if(next_htb_point):
                    sync_block_loop(host_file, fw_file, htb_points[htb_point], htb_points[next_htb_point])
                else:
                    sync_block_loop(host_file, fw_file, htb_points[htb_point], None)

                htb_point = next_htb_point

    except IOError as e: 
        print(e)
        sys.exit()

if __name__ == '__main__':
    main()