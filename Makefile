# Copyright (C) 2019  BayLibre, SAS
# Author: Fabien Parent <fparent@baylibre.com>

EXEC_PREFIX ?= "/usr"
NONARCH_BASE_LIBDIR ?= "/lib"
LIBDIR ?= "/usr/lib/"
SYSCONFDIR ?= "/etc"

ifndef IMGTEC_VERSION
$(error IMGTEC_VERSION is undefined)
endif

ifndef IMGTEC_FW
$(error IMGTEC_FW is undefined)
endif

ifndef IMGTEC_SH
$(error IMGTEC_SH is undefined)
endif


all:

install:
	install -d $(EXEC_PREFIX)
	cp -r $(IMGTEC_VERSION)/usr/include/ $(EXEC_PREFIX)

	if [ -d "$(IMGTEC_VERSION)/usr/share" ]; then \
		install -d $(EXEC_PREFIX); \
		cp -r $(IMGTEC_VERSION)/usr/share/ $(EXEC_PREFIX); \
	fi

	install -d $(LIBDIR)
	cp -r $(IMGTEC_VERSION)/usr/lib64/* $(LIBDIR)

	if [ -d "$(IMGTEC_VERSION)/etc" ]; then \
		install -d $(SYSCONFDIR); \
		cp -r ${IMGTEC_VERSION}/etc/* $(SYSCONFDIR); \
	fi

	install -d $(NONARCH_BASE_LIBDIR)/firmware
	install $(IMGTEC_VERSION)/lib/firmware/$(IMGTEC_FW) $(NONARCH_BASE_LIBDIR)/firmware/
	install $(IMGTEC_VERSION)/lib/firmware/$(IMGTEC_SH) $(NONARCH_BASE_LIBDIR)/firmware/

	if [ -f "$(IMGTEC_VERSION)/usr/local/lib/dri/pvr_dri.so" ]; then \
		install -d $(EXEC_PREFIX)/local/lib/dri; \
		install $(IMGTEC_VERSION)/usr/local/lib/dri/pvr_dri.so \
			$(EXEC_PREFIX)/local/lib/dri/; \
	fi
